### HOW TO RELEASE NEW VERSION

For nodejs,

Step 1: Open gitlab CI and add version you wanted. Eg:

```
docker-17.07.0-nodejs-9.11.2: *build_and_deploy_custom
docker-17.07.0-python-3.6.1-awscli-1.11.151-nodejs-9.11.2: *build_and_deploy_custom
```

Step 2: Open scripts/custom-docker-build and add new version in `print_nodejs_args` for nodejs:

```

```