# Changelog

## 2017-09-27, @particle4dev

* **Images**
  * Remove docker-17.07.0-nodejs-8.5.0-python-3.6.1-awscli-1.11.151 image because node is not support python 3.6.1.
  ```
  gyp ERR! stack Error: Python executable "/usr/local/bin/python" is v3.6.1, which is not supported by gyp.
  gyp ERR! stack You can pass the --python switch to point to Python >= v2.5.0 & < 3.0.0.
  ``` [#4](https://gitlab.com/particle4dev/build-images/merge_requests/4)
