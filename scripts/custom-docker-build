#!/bin/sh

###
# HOW THIS WORK
#
# 1. TRY TO PULL DOCKER IMAGES FROM GITLAB; IF exist THEN QUIT
# 2. WRITE DOCKERFILE
# 3. BUILD AND PUSH
#
# If you want to rebuild, delete images on gitlab
#
# Test
#
# ./scripts/custom-docker-build docker-17.07.0-nodejs-8.5.0 "registry.gitlab.com/particle4dev/build-images:docker-17.07.0-nodejs-8.5.0"
###

set -e
IFS=$'\n\t'

function fileExists {
if [ -f $1 ]; then
  return 0
fi
return 1
}

##
#
# writeToDockerFile
##

function writeToDockerFile {
  if ! fileExists $Dockerfile; then
    touch $Dockerfile
  fi
  unset custom_image_name
  unset custom_image_version

  for i in "$@"
  do
  case $i in
  CUSTOM_IMAGE_NAME=*)
  custom_image_name="${i#*=}"
  shift
  ;;
  CUSTOM_IMAGE_VERSION=*)
  custom_image_version="${i#*=}"
  shift
  ;;
  *)
  # unknown option
  ;;
  esac
  done

  if [ -z $custom_image_version ];
  then
    custom_image_version="latest"
  fi
  if [ ! -z $custom_image_name ];
  then
    echo "FROM $custom_image_name:$custom_image_version" >> $Dockerfile
  fi
  echo ${@} >> $Dockerfile
}

function print_python_args() {
  PYTHON_VERSION=$1

  case "$PYTHON_VERSION" in
    3.6.1)
      PYTHON_GPG_KEY=0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D
      PYTHON_PIP_VERSION=9.0.1
      ;;
    *) echo "Unknown python version $1"; exit 1;
  esac

  writeToDockerFile ""
  writeToDockerFile "ENV PYTHON_VERSION $PYTHON_VERSION"
  writeToDockerFile "ENV PYTHON_GPG_KEY $PYTHON_GPG_KEY"
  writeToDockerFile "ENV PYTHON_PIP_VERSION $PYTHON_PIP_VERSION"
  writeToDockerFile "COPY /scripts/install-python /scripts/"
  writeToDockerFile "RUN /scripts/install-python && python --version;"

}

function print_nodejs_args() {
  NODE_VERSION=$1

  case "$NODE_VERSION" in
    8.5.0)
      YARN_VERSION=1.0.2
      ;;
    8.9.3)
      YARN_VERSION=1.3.2
      ;;
    9.11.2)
      YARN_VERSION=1.5.1
      ;;
    10.16.3)
      YARN_VERSION=1.19.1
      ;;
    11.15.0)
      YARN_VERSION=1.19.1
      ;;
    12.10.0)
      YARN_VERSION=1.19.1
      ;;
    *) echo "Unknown nodejs version $1"; exit 1;
  esac
  writeToDockerFile ""
  writeToDockerFile "ENV NODE_VERSION $NODE_VERSION"
  writeToDockerFile "ENV YARN_VERSION $YARN_VERSION"
  writeToDockerFile "COPY /scripts/install-node /scripts/"
  writeToDockerFile "RUN /scripts/install-node && node --version;"
}

function print_awscli_args() {
  AWSCLI_VERSION=$1

  writeToDockerFile ""
  writeToDockerFile "ENV AWSCLI_VERSION $AWSCLI_VERSION"
  writeToDockerFile "COPY /scripts/install-awscli /scripts/"
  writeToDockerFile "RUN /scripts/install-awscli && aws --version;"
}

function parse_arguments() {
  read base
  read base_version
  printf -- "-f $Dockerfile " "$base"
  writeToDockerFile CUSTOM_IMAGE_NAME=$base CUSTOM_IMAGE_VERSION=$base_version
  writeToDockerFile "COPY /scripts/install-essentials /scripts/"
  writeToDockerFile "RUN /scripts/install-essentials"

  while read image; do
    read version
    case "$image" in
      python) print_python_args $version ;;
      awscli) print_awscli_args $version ;;
      nodejs) print_nodejs_args $version ;;
      *) exit 1;;
    esac
  done
}

function generate_command() {
  buildimage_name=$1; shift;

  printf -- "docker build "
  echo $buildimage_name | tr '-' '\n' | parse_arguments

  for i in "$@"
  do
    printf -- "%s " "$i"
  done
  printf -- ".\\n"
}

# try to build docker images
exitCode=$($CI_PROJECT_DIR/scripts/pull-docker-image | tail -n 1)
if [ $exitCode -eq "0" ]; then
  echo "FOUND $CI_REGISTRY_IMAGE:$CI_JOB_NAME"; exit 0;
fi

Dockerfile="$CI_PROJECT_DIR/Dockerfile"

# Empty file
if fileExists $Dockerfile; then
  cp /dev/null $Dockerfile
fi

docker_command=$(generate_command $@)
echo "$1: executing $docker_command"
eval $docker_command
